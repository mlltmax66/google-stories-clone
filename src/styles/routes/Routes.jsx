import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { NavBar } from '../../components'
import Home from '../../pages/Home'

const Routes = () => {
    return (
        <>
            <NavBar />
            <Switch>
                <Route exact path="/" component={Home} />
            </Switch>
        </>
    )
}

export default Routes
