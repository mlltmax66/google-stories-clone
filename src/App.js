import React, { useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Routes from './styles/routes/Routes';
import { gsap } from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger';

function App() {

  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger)
  }, [])

  return (
    <Router>
      <Routes />
    </Router>
  );
}

export default App