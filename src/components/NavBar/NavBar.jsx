import React from 'react'
import { Link } from 'react-router-dom'
import googleIcon from '../../assets/google.png'

const NavBar = () => {
    return (
        <header className="navbar">
            <div className="navbar-brand">
                <Link to="/" className="navbar-title">Web Stories on</Link>
                <img className="navbar-logo" src={googleIcon} alt="Logo" />
            </div>
            <div className="navbar-wrapper">
                <nav className="navbar-nav">
                    <p className="navbar-nav-link">How it works</p>
                    <p className="navbar-nav-link">Tools</p>
                    <p className="navbar-nav-link">Best practices</p>
                    <p className="navbar-nav-link">Showcase</p>
                    <p className="navbar-nav-link">Resources</p>
                </nav>
                <button className="button primary">Create a story</button>
            </div>
        </header>
    )
}

export default NavBar
