import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import useOnScreen from '../../hooks/useOnScreen'

const Slide = ({ title, description, color, index, updateActiveImage }) => {
    const ref = useRef(null)
    const onScreen = useOnScreen(ref)

    useEffect(() => {
        if(onScreen) {
            updateActiveImage(index)
        }
        //eslint-disable-next-line
    }, [onScreen, index])

    return (
        <div ref={ref} className="slide">
            {index === 0 && <h2 className="slide-big-title">What hosting your own Web Story makes possible:</h2>}
            <span style={{ backgroundColor: color }}></span>
            <h3 className="slide-title">
                { title }
            </h3>
            <p className="slide-description">
                { description }
            </p>
            {index === 3 && <button className="slide-button button primary">See how it works</button>}
        </div>
    )
}

Slide.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    updateActiveImage: PropTypes.func.isRequired,
}

export default Slide