import React, { useEffect, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { slides } from '../../data'
import { gsap } from 'gsap'
import Slide from './Slide'

const RenderVideos = ({ activeIndex }) => {
    return slides.map(({videoUrl, color}, idx) => (
        <div style={{ backgroundColor: `${color}` }} key={idx} className={activeIndex === idx ? "slide-wrapper as-primary" : "slide-wrapper"}>       
            <video 
                className="slide-video"
                playsInline=" "
                autoPlay
                loop
                src={videoUrl}
            ></video>
        </div>
    ))
}

RenderVideos.protoTypes = {
    activeIndex: PropTypes.number.isRequired,
}

const Slides = () => { 
    const [activeIndex, setActiveIndex] = useState(0)
    const sliderRef = useRef(null)
    const slidesRightRef = useRef(null)

    useEffect(() => {
        const stopTrigger = () => {
            const tl = gsap.timeline({
                scrollTrigger: {
                    trigger: slidesRightRef.current,
                    start: "top top",
                    end: () => `+=${sliderRef.current.offsetHeight}`,
                    scrub: true,
                    pin: true,
                }
            })
            return tl
        }

        const main = gsap.timeline()
        setTimeout(() => {
            main.add(stopTrigger())
        }, 1000)
    }, [])

    return (
        <section ref={sliderRef} className="slides-container">
            <div className="slides-left">
                {slides.map((slide, idx) => (
                    <Slide 
                        key={idx}
                        title={slide.title}
                        description={slide.description}
                        color={slide.color}
                        index={idx}
                        updateActiveImage={setActiveIndex}
                    />
                ))}
            </div>
            <div ref={slidesRightRef} className="slides-right">
                <RenderVideos activeIndex={activeIndex} />
            </div>
        </section>
    )
}

export default Slides
