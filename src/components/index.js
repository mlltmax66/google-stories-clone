import NavBar from "./NavBar/NavBar"
import Hero from './Hero/Hero'
import Slides from './Slides/Slides'

export { NavBar, Hero, Slides }