import React from 'react'
import HeroCollage from './HeroCollage'
import HeroFooter from './HeroFooter'
import HeroHeader from './HeroHeader'
import HeroPhoneBlock from './HeroPhoneBlock'
import HeroUsedBy from './HeroUsedBy'

const Hero = () => {
    return (
        <section className="hero">
            <HeroHeader />
            <div className="hero-container">
                <HeroCollage />
                <HeroPhoneBlock />
            </div>
            <HeroUsedBy />
            <HeroFooter />
        </section>
    )
}

export default Hero
