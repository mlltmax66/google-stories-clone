import React, { useRef, useEffect } from 'react'
import imgSrc from '../../assets/hero-phone-frame-v2.png'
import { gsap } from 'gsap'
const videoUrl = "https://kstatic.googleusercontent.com/files/c44f15bb7e678d651e18fdee3058f2948aa733849e0dea3daf7429bf0f77ec23bd670dba63e71739d5b53489c98689bdbb80c47cf55f44649d9d1bfdf3e4f0a0"

const HeroPhoneBlock = () => {
    const phoneRef = useRef(null)

    useEffect(() => {
        const intro = () => {
            const tl = gsap.timeline()

            tl.fromTo(phoneRef.current, { y: 200 }, { y: 0, duration: 1 })
        }

        const stopTrigger = () => {
            const tl = gsap.timeline({
                delay: 1,
                scrollTrigger: {
                    trigger: phoneRef.current,
                    start: "top top",
                    end: "+=960",
                    pin: true,
                    scrub: true
                }
            })
            tl.to(phoneRef.current, { scale: 1.2 }, "+=.2")
            tl.to(".hero", {
                backgroundColor: '#212225',
                duration: .25
            }, "-=.5")
            return tl
        }

        const main = gsap.timeline()
        main.add(intro())
        setTimeout(() => {
            main.add(stopTrigger())
        }, 1000)
    }, [])

    return (
        <div className="hero-phone-black" ref={phoneRef}>
            <div className="hero-phone-template" style={{ backgroundImage: `url(${imgSrc})` }}> 
                 <video
                    className="collage-element" 
                    playsInline=" "
                    autoPlay 
                    loop
                    src={videoUrl}></video>
            </div>
        </div>
    )
}

export default HeroPhoneBlock
