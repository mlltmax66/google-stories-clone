import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { photos, videos } from '../../data'
import { gsap } from 'gsap'

const VideoElement = ({ src }) => {
    return <div className="hero-element">
        <video 
            className="collage-element" 
            playsInline=" "
            autoPlay=" "
            loop
            src={src}
        ></video>
    </div>
}

VideoElement.propTypes = {
    src: PropTypes.string.isRequired
}

const ImageElement = ({ src }) => {
    return <div className="hero-element">
        <img src={src} className="collage-element" alt=""/>
    </div>
}

ImageElement.propTypes = {
    src: PropTypes.string.isRequired
}

const HeroCollage = () => {
    const leftImages = photos.slice(0, 2)
    const rightImages = photos.slice(2, photos.length)

    const [leftVideo, rightVideo] = videos

    useEffect(() => {
        const tl = gsap.timeline({
            delay: .5
        })

        tl.fromTo(
            ".hero-element",
            { y: 300 },
            { y: 0, duration: 1, delay: .2}
        )
    }, [])

    return (
        <div className="hero-collage">
            <div>
                <div className="left-column">
                    {leftImages.map((src, idx) => (
                        <ImageElement key={idx} src={src} />
                    ))}
                    <VideoElement src={leftVideo} />
                </div>
                <div className="right-column">
                    <VideoElement src={rightVideo} />
                    {rightImages.map((src, idx) => (
                        <ImageElement key={idx} src={src} />
                    ))}
                </div>
            </div>
        </div>
    )
}

export default HeroCollage
