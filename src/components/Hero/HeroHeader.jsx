import React, { useEffect, useRef } from 'react'
import heroLogo from '../../assets/hero-logo.png'
import { gsap } from 'gsap'

const HeroHeader = () => {
    const titleRef = useRef(null)

    useEffect(() => {
        gsap.fromTo(
            titleRef.current,
            { opacity: 0 },
            { opacity: 1, duration: .5, delay: 1.2 }
        )
    }, [])

    return (
        <div className="hero-header">
            <img className="hero-header-img" src={heroLogo} alt="hero logo" />
            <h1 ref={titleRef} className="hero-header-title"><span>Stories </span>meet their<br /> widest audience ever.</h1>
        </div>
    )
}

export default HeroHeader
