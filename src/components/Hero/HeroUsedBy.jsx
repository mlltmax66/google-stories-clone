import React from 'react'
import logo1 from '../../assets/logo-vice.png'
import logo2 from '../../assets/logo-bustle.png'
import logo3 from '../../assets/logo-group.png'
import logo4 from '../../assets/logo-planet.png'
import logo5 from '../../assets/logo-gannett.png'
import logo6 from '../../assets/logo-forbes.png'
import logo7 from '../../assets/logo-ndtv.png'
import logo8 from '../../assets/logo-conde.png'
import logo9 from '../../assets/logo-times.png'
import logo10 from '../../assets/logo-folha.png'
import logo11 from '../../assets/logo-uol.png'
import logo12 from '../../assets/logo-globo.png'

const HeroUsedBy = () => {
    return (
        <div className="hero-usedBy">
            <div className="hero-usedBy-copy">
                <p>The tappable story format has never been more accessible—to creators and readers alike. See what happens when Google brings stories to the open web.</p>
            </div>
            <div className="hero-usedBy-logos">
                <p>Marketing and impact at </p>
                <div className="hero-usedBy-logos-group">
                    <img src={logo1} alt="logo vice" />
                    <img src={logo2} alt="logo bustle digital group" />
                    <img src={logo3} alt="logo group nine" />
                    <img src={logo4} alt="logo lonely planet" />
                    <img src={logo5} alt="logo gannett" />
                    <img src={logo6} alt="logo forbes" />
                    <img src={logo7} alt="logo ndtv" />
                    <img src={logo8} alt="logo condé nast" />
                    <img src={logo9} alt="logo the times of india" />
                    <img src={logo10} alt="logo folha" />
                    <img src={logo11} alt="logo uol" />
                    <img src={logo12} alt="logo editoria globo" />
                </div>
            </div>
        </div>
    )
}

export default HeroUsedBy
