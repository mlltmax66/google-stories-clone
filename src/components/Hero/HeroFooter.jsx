import React, { useRef, useEffect } from 'react'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

const HeroFooter = () => {
    const phoneRef = useRef(null)

    useEffect(() => {
        gsap.registerPlugin(ScrollTrigger)
        const tl = gsap.timeline({
            scrollTrigger: {
                trigger: phoneRef.current,
                start: "top bottom",
                scrub: true,
            },
        });
        
        tl.to(
            ".hero",
            {
                backgroundColor: "white", 
                duration: .7, 
            },
            "-=2"
        )
    }, [])

    return (
        <div ref={phoneRef} className="hero-footer">
            <h2>Visual stories that feel like<br /> yours, because they are.</h2>
        </div>
    )
}

export default HeroFooter
