import React from 'react'
import { Hero, Slides } from '../components'

const Home = () => {
    return (
        <main>
            <Hero />
            <Slides />
        </main>
    )
}

export default Home
