# Project google stories clone

This application is a clone of google stories landing page

## Dependencies
   - yarn or npm,
   - react-router-dom,
   - node-sass,
   - gsap

## Installation

Step 1 : Clone this project

```bash
git clone https://gitlab.com/mlltmax66/google-stories-clone
``` 

Step 2 : Go to the project folder

```bash
cd google-stories-clone 
```

Step 3 : Install all dependencies

```bash
yarn install
```

Step 4 : Run app

```bash
yarn start
```
